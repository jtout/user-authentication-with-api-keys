<?php

	namespace App\Middleware;
	use App\Interfaces\Singleton;
    use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;

	final class Authentication implements Singleton
	{
	    static private $__instance = NULL;

		public static function initialize() :Authentication
		{
			try
			{
				if(self::$__instance == NULL)
					self::$__instance = new Authentication();

				return self::$__instance;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function __invoke(Request $request, Response $response, callable $next) :Response
		{
			try
            {
				$apiKey = isset($request->getParams()['api_key']) ? $request->getParams()['api_key'] : false;
				$token  = !empty($request->getHeader('token')[0]) ? $request->getHeader('token')[0] : false;

				if(empty($apiKey) && empty($token))
					return $response->withJson('Unauthorized access! No api key or token provided', 401);

				$request = $request->withAttribute('api_key', $apiKey);
				$request = $request->withAttribute('token', $token);

                return $next($request, $response);
            }
            catch(\Exception $e)
            {
                throw $e;
            }
		}
	}