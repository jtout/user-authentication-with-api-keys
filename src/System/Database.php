<?php
	namespace App\System;
	use App\Interfaces\Singleton;

	final class Database implements Singleton
	{
		static private $__host 		= 'localhost';
		static private $__dbName 	= 'UserApiKeys';
		static private $__charset 	= 'utf8mb4';
		static private $__dbUser 	= '';
		static private $__dbPass 	= '';
		static private $__instance  = NULL;

		private function __construct() {}

		public static function initialize() :\PDO
		{
			try
			{
				if(self::$__instance == NULL)
					self::$__instance = self::__connect();

				return self::$__instance;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		private static function __connect() :\PDO
		{
			try
			{
				$pdo = new \PDO('mysql:host=' . self::$__host . ';dbname=' . self::$__dbName . ';charset=' . self::$__charset . ';connect_timeout=15', self::$__dbUser, self::$__dbPass);
				$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);

				return $pdo;
			}
			catch (\PDOException $e)
			{
				throw $e;
			}
		}
	}