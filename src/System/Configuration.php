<?php
	namespace App\System;
	use App\Interfaces\Singleton;

	final class Configuration implements Singleton
	{
		private $__db;
		static private $__instance = NULL;

		private function __construct()
		{
			try
			{
				$this->__db = Database::initialize();
				$this->__loadSettings();
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public static function initialize() :Configuration
		{
			try
			{
				if(self::$__instance == NULL)
					self::$__instance = new Configuration();

				return self::$__instance;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		private function __loadSettings()
		{
			try
			{
				ini_set('error_reporting', E_ALL);
				ini_set('display_errors',1);

				date_default_timezone_set('Europe/Athens');

				setlocale(LC_MONETARY, 'el_GR.UTF-8');
				mb_internal_encoding('UTF-8');
				mb_regex_encoding('UTF-8');
				mb_http_output('UTF-8');

                if (session_status() == PHP_SESSION_NONE)
                    session_start();
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function db() :\PDO
		{
			try
			{
				return $this->__db;
			}
			catch (\PDOException $e)
			{
				throw $e;
			}
		}
	}