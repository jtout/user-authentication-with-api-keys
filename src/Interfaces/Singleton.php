<?php

	namespace App\Interfaces;

	interface Singleton
	{
		public static function initialize();
	}