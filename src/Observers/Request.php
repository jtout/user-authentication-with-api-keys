<?php
	namespace App\Observers;
	use App\Entities\LogEntity;
	use App\Interfaces\Observable;
	use App\Interfaces\Observer;
	use App\Models\Log;
	use App\System\Configuration;

	class Request implements Observer
	{
		protected $_data;

		public function __construct(Observable $observable, array $data)
		{
			try
			{
				$observable->attach($this);
				$this->_data = $data;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function update(Observable $observable)
		{
			try
			{
				$configuration = Configuration::initialize();

				$entityData = array(
					'user_id' => isset($this->_data['user_id']) ? $this->_data['user_id'] : null,
					'api_key' => isset($this->_data['api_key']) ? $this->_data['api_key'] : null,
                    'token'   => isset($this->_data['token'])   ? $this->_data['token']   : null,
					'message' => isset($this->_data['message']) ? $this->_data['message'] : null
				);

				$logEntity = new LogEntity($entityData);
				$log = new Log($configuration->db());
				$log->logRequest($logEntity);
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}
	}