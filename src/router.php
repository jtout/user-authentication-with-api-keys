<?php
	use \App\Middleware\Authentication;

	$app->get('/user/{id}', '\App\Controllers\Controller:processRequest')->add(Authentication::initialize());
