<?php

    namespace App\Entities;


    class UserEntity
    {
        protected $_id;
        protected $_user_name;
		protected $_api_key;
		protected $_token;
		protected $_token_expire;

		public function __construct(array $data)
        {
            if(isset($data['id']))
                $this->_id = $data['id'];

            if(isset($data['user_name']))
                $this->_user_name = $data['user_name'];

			if(isset($data['api_key']))
				$this->_api_key = $data['api_key'];

            if(isset($data['token']))
                $this->_token = $data['token'];

            if(isset($data['token_expire']))
                $this->_token_expire = $data['token_expire'];
        }

        public function id() :int
        {
			if($this->_id == null)
				$this->_id = 0;

			return $this->_id;
        }

        public function username() :string
        {
			if($this->_user_name == null)
				$this->_user_name = '';

			return $this->_user_name;
        }

		public function api_key() :string
		{
			if($this->_api_key == null)
				$this->_api_key = '';

			return $this->_api_key;
		}

        public function token() :string
        {
            if($this->_token == null)
                $this->_token = '';

            return $this->_token;
        }

        public function token_expire() :string
        {
            if($this->_token_expire == null)
                $this->_token_expire = '';

            return $this->_token_expire;
        }
    }