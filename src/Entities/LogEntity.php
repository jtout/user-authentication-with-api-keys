<?php

    namespace App\Entities;


    class LogEntity
    {
        protected $_id;
        protected $_user_id;
		protected $_api_key;
        protected $_token;
		protected $_message;

		public function __construct(array $data)
        {
            if(isset($data['id']))
                $this->_id = $data['id'];

            if(isset($data['user_id']))
                $this->_user_id = $data['user_id'];

			if(isset($data['api_key']))
				$this->_api_key = $data['api_key'];

            if(isset($data['token']))
                $this->_token = $data['token'];

			if(isset($data['message']))
				$this->_message = $data['message'];
        }

        public function id() :int
        {
			return $this->_id;
        }

        public function user_id() :int
        {
			if($this->_user_id == null)
				$this->_user_id = 0;

			return $this->_user_id;
        }

		public function api_key() :string
		{
			if($this->_api_key == null)
				$this->_api_key = '';

			return $this->_api_key;
		}

        public function token() :string
        {
            if($this->_token == null)
                $this->_token = '';

            return $this->_token;
        }

		public function message() :string
		{
			if($this->_message == null)
				$this->_message = '';

			return $this->_message;
		}
    }