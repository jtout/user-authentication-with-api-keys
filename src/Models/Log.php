<?php

	namespace App\Models;

    use App\Entities\LogEntity;

	class Log extends Model
	{
		public function logRequest(LogEntity $logEntity)
		{
			try
			{
				$this->_db->beginTransaction();

				$sql  = "
					INSERT INTO log
						(
							user_id,
							api_key,
							token,
							message,
							created_on
						)
					VALUES 
						(
							:user_id,
							:api_key,
							:token,
							:message,
							NOW()
						)
				";

				$stmt  = $this->_db->prepare($sql);
				$stmt->bindValue(':user_id', $logEntity->user_id(), \PDO::PARAM_INT);
				$stmt->bindValue(':api_key', $logEntity->api_key(), \PDO::PARAM_STR);
                $stmt->bindValue(':token',   $logEntity->token(),   \PDO::PARAM_STR);
				$stmt->bindValue(':message', $logEntity->message(), \PDO::PARAM_STR);
				$stmt->execute();

				$this->_db->commit();
			}
			catch (\PDOException $e)
			{
				$this->_db->rollBack();

				throw $e;
			}
		}
	}