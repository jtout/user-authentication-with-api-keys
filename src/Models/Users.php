<?php

	namespace App\Models;

	use App\Entities\UserEntity;

    class Users extends Model
	{
		public function findUser(int $id, string $key) :UserEntity
        {
            try
            {
                $sql  = "SELECT
						    a.id,
                            a.user_name,
                            a.api_key
                        FROM
                            users AS a
                        WHERE
                            a.id = :id
                ";

                $stmt  = $this->_db->prepare($sql);
                $stmt->bindParam(':id', $id, \PDO::PARAM_INT);
                $stmt->execute();

                $userData = $stmt->fetch(\PDO::FETCH_ASSOC);

                if(!empty($userData['id']) && $key === $userData['api_key'])
                {
                    $token = password_hash($userData['api_key'], PASSWORD_BCRYPT);
                    $token_expire = date('Y-m-d H:i:s', strtotime('+1 hour'));

                    $sql  = "  UPDATE
                                users
                              SET 
                                token = :token,
                                token_expire = :token_expire
                              WHERE
                                id = :id 
                    ";

                    $stmt  = $this->_db->prepare($sql);
                    $stmt->bindParam(':token', $token, \PDO::PARAM_STR);
                    $stmt->bindParam(':token_expire', $token_expire, \PDO::PARAM_STR);
                    $stmt->bindParam(':id', $userData['id'], \PDO::PARAM_INT);
                    $stmt->execute();


                    $userData['token']  = $token;
                    $userData['token_expire'] = date('Y-m-d H:i:s', strtotime('+1 hour'));
                }
                else
                {
                    $userData = array(
                        'id' => null,
                        'api_key' => $key
                    );
                }

                $user = new UserEntity($userData);

                return $user;
            }
            catch(\PDOException $e)
            {
                throw $e;
            }
        }

        public function findUserById(int $id, string $token = '') :UserEntity
        {
            try
            {
                $sql  = "SELECT
						    a.*
                        FROM
                            users AS a
                        WHERE
                            a.id = :id
                ";

				if(!empty($token))
					$sql .= ' AND a.token = :token';

                $stmt  = $this->_db->prepare($sql);
                $stmt->bindParam(':id', $id, \PDO::PARAM_INT);

				if(!empty($token))
					$stmt->bindParam(':token', $token, \PDO::PARAM_STR);

                $stmt->execute();

                $userData = $stmt->fetch(\PDO::FETCH_ASSOC);

				if(empty($userData['id']))
					$userData = array('id' => 0, 'token' => $token);

                $user = new UserEntity($userData);

                return $user;
            }
            catch (\PDOException $e)
            {
                throw $e;
            }
        }

        public function verifyUserByToken(int $userId, string $token) :bool
        {
            try
            {
                $sql  = "SELECT
						    a.id
                        FROM
                            users AS a
                        WHERE
                            a.token = :token AND
                            a.token_expire >= NOW() AND 
                            a.id = :id
                ";

                $stmt  = $this->_db->prepare($sql);
                $stmt->bindParam(':token', $token, \PDO::PARAM_STR);
                $stmt->bindParam(':id', $userId, \PDO::PARAM_INT);
                $stmt->execute();

                $userData = $stmt->fetch(\PDO::FETCH_ASSOC);

                if(empty($userData['id']))
                {
                    return false;
                }
                else
                    $this->__updateUserTokenTime($userData['id']);

                return true;
            }
            catch(\PDOException $e)
            {
                throw $e;
            }
        }

        private function __updateUserTokenTime(int $userId)
        {
            try
            {
                $expire = date('Y-m-d H:i:s', strtotime('+1 hour'));

                $sql  = "  UPDATE
                            users
						  SET token_expire = :token_expire
                          WHERE
                            id = :id 
                ";

                $stmt  = $this->_db->prepare($sql);
                $stmt->bindParam(':token_expire', $expire, \PDO::PARAM_STR);
                $stmt->bindParam(':id', $userId, \PDO::PARAM_INT);
                $stmt->execute();
            }
            catch(\PDOException $e)
            {
                throw $e;
            }
        }
	}