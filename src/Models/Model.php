<?php

	namespace App\Models;

	class Model
	{
		protected $_db;

		public function __construct($db)
		{
			try
			{
				$this->_db = $db;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}
	}