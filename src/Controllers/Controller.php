<?php

	namespace App\Controllers;
    use App\Interfaces\Observable;
	use App\Interfaces\Observer;
	use App\Models\Users;
    use \Psr\Http\Message\ServerRequestInterface as Request;
	use \Psr\Http\Message\ResponseInterface as Response;
	use App\Observers\Request as RequestObserver;

	class Controller implements Observable
	{
		protected $_ci;
		private $__observer;

		public function __construct(\Slim\Container $ci)
		{
			try
            {
                $this->_ci = $ci;
            }
            catch(\Exception $e)
            {
                throw $e;
            }
		}

		public function attach(Observer $observer)
		{
			try
			{
				$this->__observer = $observer;
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function detach(Observer $observer)
		{
			try
			{
				if($this->__observer === $observer)
					unset($this->__observer);
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function notify(Observer $observer)
		{
			try
			{
				return $observer->update($this);
			}
			catch (\Exception $e)
			{
				throw $e;
			}
		}

		public function processRequest(Request $request, Response $response, array $args) :Response
		{
			try
            {
				$responseData = array(
					'public_info' => array(),
					'dev_info' => array(),
					'response_code' => 200
				);

				$id = filter_var($args['id'], FILTER_VALIDATE_INT);
                $apiKey = filter_var($request->getAttribute('api_key'), FILTER_SANITIZE_STRING);
                $token = filter_var($request->getAttribute('token'), FILTER_SANITIZE_STRING);

                $users = new Users($this->_ci->db);

                if(!empty($apiKey))
                {
                    $user = $users->findUser($id, $apiKey);

                    if(!empty($user->id()))
                    {
                        $responseData = array(
                            'public_info' => array(
                                'user_id' => $user->id(),
                                'user_name' => $user->username(),
                                'message' =>  'User Found!',
                            ),
                            'dev_info' => array(
                                'message' =>  'User Found!',
                                'user_id' => $user->id(),
                                'api_key' => $user->api_key(),
                            ),
                            'response_code' => 200
                        );
                        $response = $response->withHeader('token', $user->token());
                    }
                    else
                    {
                        $responseData = array(
                            'public_info' => array(
                                'user_id' => $id,
                                'message' =>  'User not Found or invalid api key!',
                            ),
                            'dev_info' => array(
                                'message' =>  'User not Found for the provided api key.',
                                'user_id' => $id,
                                'api_key' => $apiKey,
                            ),
                            'response_code' => 200
                        );
                    }
                }
                else if(empty($apiKey) && !empty($token))
                {
                    $user = $users->findUserById($id, $token);
                    if(!empty($user->id()))
                    {
                        $responseData = array(
                            'public_info' => array(
                                'user_id' => $user->id(),
                                'user_name' => $user->username(),
                                'message' =>  'User Found!',
                            ),
                            'dev_info' => array(
                                'message' =>  'User Found!',
                                'user_id' => $user->id(),
                                'token' => $user->token(),
                            ),
                            'response_code' => 200
                        );

                        $response = $response->withHeader('token', $user->token());
                    }
                    else
                    {
                        $responseData = array(
                            'public_info' => array(
                                'user_id' => '',
                                'message' =>  'Your session has expired. Please try again providing your api key!',
                            ),
                            'dev_info' => array(
                                'message' =>  'Invalid Token or token has expired',
                                'user_id' => $id,
                                'token' => $token,
                            ),
                            'response_code' => 200
                        );
                    }
                }

				$observer = new RequestObserver($this, $responseData['dev_info']);
				$this->notify($observer);
				$this->detach($observer);

				return $response->withJson(
                    $responseData['public_info'],
                    $responseData['response_code']
				);
            }
            catch(\Exception $e)
            {
                throw $e;
            }
		}
	}