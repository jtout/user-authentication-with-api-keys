# README #

### What is this repository for? ###

A simple REST Api for retrieving user's information.

### Requirements ###
PHP 7

### How do I get set up? ###

1) Run composer update in order to download dependencies.  
2) Fill in DB connection details in src/System/Database.php  
3) Run demo.sql file to create demo users and log tables

### How to use ###

For the first request you have to provide the user's Api key. If user exists in database, api will generate a random unique token that it will be provided through response headers.  

Example Request: curl -v http://localhost/public_html/user/1?api_key=APIKEYTEST  

Example Response:  
< HTTP/1.1 200 OK  
< Date: Tue, 05 Jun 2018 05:46:55 GMT  
< Server: Apache/2.4.33 (FreeBSD) PHP/7.2.5  
< X-Powered-By: PHP/7.2.5  
< Set-Cookie: PHPSESSID=60b24f3adc278b93ef6a72125f2d0a13; path=/  
< Expires: Thu, 19 Nov 1981 08:52:00 GMT  
< Cache-Control: no-store, no-cache, must-revalidate  
< Pragma: no-cache  
< token: $2y$10$QfcDqX/WFb/CfWj/GQIsXunZrNBUXOyoke7cCUEFyLRfm6Uy6BG.G  
< Content-Length: 60  
< Content-Type: application/json;charset=utf-8  
<
Connection #0 to host <host> left intact  
{"user_id":1,"user_name":"demouser","message":"User Found!"}  

From the second request if you pass the token generated user will be authenticated without credentials.  

Example Request: curl -v -h 'token: $2y$10$QfcDqX/WFb/CfWj/GQIsXunZrNBUXOyoke7cCUEFyLRfm6Uy6BG.G' http://localhost/public_html/user/1
