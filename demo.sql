SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `token` varchar(255) NOT NULL,
  `token_expire` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES (1, 'demouser', 'APIKEYTEST', '$2y$10$n5kvqmji6JlpAhzjS3RB7ueopTjBFJKe4jNU1ASnCqmyPeU7U7LUa', '2018-06-01 17:19:00');
INSERT INTO `users` VALUES (2, 'testuser', 'TESTAPIKEY', '', NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
