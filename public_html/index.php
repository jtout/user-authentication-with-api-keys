<?php
	require_once '../vendor/autoload.php';

	$configuration = \App\System\Configuration::initialize();

	$app = new \Slim\App(new \Slim\Container([
        'settings' => [
            'displayErrorDetails' => true,
            'determineRouteBeforeAppMiddleware' => true,
        ]
    ]));

	$container 			 = $app->getContainer();
	$container['db']	 = $configuration->db();

    require_once '../src/router.php';

	$app->run();